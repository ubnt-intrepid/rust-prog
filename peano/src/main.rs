extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate rustyline;

mod parse;
mod peano;

use rustyline::error::ReadlineError;
use rustyline::Editor;
use parse::Ast;
use peano::Nat;

fn eval(ast: &Ast) -> Nat {
    match *ast {
        Ast::Nat(ref nat) => nat.clone(),
        Ast::Add(ref lhs, ref rhs) => eval(lhs).add(&eval(rhs)),
        Ast::Mul(ref lhs, ref rhs) => eval(lhs).mul(&eval(rhs)),
    }
}

fn main() {
    let mut readline = Editor::<()>::new();
    let _ = readline.load_history("history.txt");
    loop {
        match readline.readline(">> ") {
            Ok(line) => {
                if line.trim().is_empty() {
                    continue;
                }
                let ast = match parse::parse(&line) {
                    Ok(ast) => {
                        println!("Parsed: {:?}", ast);
                        ast
                    }
                    Err(err) => {
                        println!("Parse Error:\n{}", err);
                        continue;
                    }
                };

                println!("Evaluated: {}", eval(&ast).to_u32());

                readline.add_history_entry(&line);
            }
            Err(ReadlineError::Interrupted) => {
                println!("Interrupted");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("Eof");
                break;
            }
            Err(err) => panic!("{:?}", err),
        }
    }
    readline.save_history("history.txt").unwrap();
}
