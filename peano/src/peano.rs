#[derive(Debug, Clone)]
#[cfg_attr(test, derive(PartialEq))]
pub enum Nat {
    Zero,
    Succ(Box<Nat>),
}

impl Nat {
    pub fn zero() -> Self {
        Nat::Zero
    }

    pub fn succ(&self) -> Self {
        Nat::Succ(Box::new(self.clone()))
    }

    pub fn to_u32(&self) -> u32 {
        match *self {
            Nat::Zero => 0,
            Nat::Succ(ref prev) => 1 + prev.to_u32(),
        }
    }

    pub fn add(&self, rhs: &Nat) -> Nat {
        match *rhs {
            Nat::Zero => self.clone(),
            Nat::Succ(ref prev) => self.add(&*prev as &Nat).succ(),
        }
    }

    pub fn mul(&self, rhs: &Nat) -> Nat {
        match *rhs {
            Nat::Zero => Nat::Zero,
            Nat::Succ(ref prev) => self.mul(prev).add(self),
        }
    }
}

#[cfg(test)]
macro_rules! matches {
    ($lhs:expr, $($rhs:tt)+) => {
        match $lhs {
            $($rhs)+ => true,
            _ => false,
        }
    }
}

#[test]
fn test_zero() {
    assert!(matches!(Nat::zero(), Nat::Zero));
}

#[test]
fn test_succ() {
    assert!(matches!(Nat::zero().succ(), Nat::Succ(_)));
}

#[test]
fn test_to_u32_zero() {
    assert_eq!(Nat::zero().to_u32(), 0);
}

#[test]
fn test_to_u32_one() {
    assert_eq!(Nat::zero().succ().to_u32(), 1);
}

#[test]
fn test_add() {
    let a = Nat::zero();
    let b = Nat::zero().succ().succ();
    assert_eq!(a.add(&b).to_u32(), 2);
}

#[test]
fn test_add_1() {
    let a = Nat::zero().succ();
    let b = Nat::zero();
    assert_eq!(a.add(&b).to_u32(), 1);
}

#[test]
fn test_add_2() {
    let a = Nat::zero().succ();
    let b = Nat::zero().succ().succ();
    assert_eq!(a.add(&b).to_u32(), 3);
}

#[test]
fn test_mul() {
    let a = Nat::zero().succ();
    let b = Nat::zero();
    assert_eq!(a.mul(&b).to_u32(), 0);
}

#[test]
fn test_mul_1() {
    let a = Nat::zero();
    let b = Nat::zero().succ().succ();
    assert_eq!(a.mul(&b).to_u32(), 0);
}

#[test]
fn test_mul_2() {
    let a = Nat::zero().succ().succ();
    let b = Nat::zero().succ().succ().succ();
    assert_eq!(a.mul(&b).to_u32(), 6);
}
