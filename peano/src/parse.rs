use pest::Parser;
use pest::iterators::Pair;
use pest::inputs::StrInput;
use peano::Nat;

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub enum Ast {
    Nat(Nat),
    Add(Box<Ast>, Box<Ast>),
    Mul(Box<Ast>, Box<Ast>),
}

impl Ast {
    fn add(lhs: Ast, rhs: Ast) -> Self {
        Ast::Add(Box::new(lhs), Box::new(rhs))
    }

    fn mul(lhs: Ast, rhs: Ast) -> Self {
        Ast::Mul(Box::new(lhs), Box::new(rhs))
    }
}

#[derive(Parser)]
#[grammar = "grammar.pest"]
struct AstParser;

pub fn parse(input: &str) -> Result<Ast, String> {
    let pair = AstParser::parse_str(Rule::rule, input)
        .map_err(|e| e.to_string())?
        .next()
        .ok_or_else(|| "rule is empty")?;
    Ok(parse_ast(pair))
}

macro_rules! into_inner {
    ($pair:expr) => { $pair.into_inner().filter(|p| p.as_rule() != Rule::sp).next().unwrap() }
}

fn parse_ast(pair: Pair<Rule, StrInput>) -> Ast {
    match pair.as_rule() {
        Rule::mul => {
            match parse_binop(pair) {
                (lhs, rhs) => Ast::mul(lhs, rhs),
            }
        }
        Rule::add => {
            match parse_binop(pair) {
                (lhs, rhs) => Ast::add(lhs, rhs),
            }
        }
        Rule::nat => parse_ast(into_inner!(pair)),
        Rule::zero => Ast::Nat(Nat::zero()),
        Rule::succ => Ast::Nat(parse_succ(into_inner!(pair)).succ()),
        _ => unreachable!(),
    }
}

fn parse_binop(pair: Pair<Rule, StrInput>) -> (Ast, Ast) {
    let mut pairs = pair.into_inner().filter(|p| p.as_rule() != Rule::sp);
    let lhs = pairs.next();
    let rhs = pairs.next();
    match (lhs, rhs) {
        (Some(lhs), Some(rhs)) => (parse_ast(lhs), parse_ast(rhs)),
        _ => unreachable!(),
    }
}

fn parse_succ(pair: Pair<Rule, StrInput>) -> Nat {
    match pair.as_rule() {
        Rule::nat => parse_succ(into_inner!(pair)),
        Rule::succ => parse_succ(into_inner!(pair)).succ(),
        Rule::zero => Nat::zero(),
        _ => unreachable!(),
    }
}

#[test]
fn test_parse_zero() {
    let input = "Zero";
    assert_eq!(parse(input).unwrap(), Ast::Nat(Nat::zero()));
}

#[test]
fn test_parse_succ() {
    let input = "Succ(Succ(Zero))";
    assert_eq!(parse(input).unwrap(), Ast::Nat(Nat::zero().succ().succ()));
}

#[test]
fn test_parse_add() {
    let input = "Zero + Succ(Zero)";
    let output = parse(input).unwrap_or_else(|e| panic!("\n{}\n", e));
    assert_eq!(
        output,
        Ast::add(Ast::Nat(Nat::zero()), Ast::Nat(Nat::zero().succ()))
    );
}

#[test]
fn test_parse_nested() {
    let input = "Zero * (Succ(Zero) + Zero)";
    let output = parse(input).unwrap_or_else(|e| panic!("\n{}\n", e));
    assert_eq!(
        output,
        Ast::mul(
            Ast::Nat(Nat::zero()),
            Ast::add(Ast::Nat(Nat::zero().succ()), Ast::Nat(Nat::zero())),
        )
    );
}
