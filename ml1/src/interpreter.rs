#![allow(dead_code)]

pub trait Parser {
    type Ast;
    type Err;

    fn parse(&self, input: &str) -> Result<Self::Ast, Self::Err>;
}

pub trait Eval {
    type Ast;
    type Out;
    type Err;

    fn eval(&self, ast: Self::Ast) -> Result<Self::Out, Self::Err>;
}

pub struct Interpreter<P, E>(P, E);

impl<P, E> Interpreter<P, E>
where
    P: Parser,
    E: Eval<Ast = P::Ast, Err = P::Err>,
{
    pub fn new(parser: P, eval: E) -> Self {
        Interpreter(parser, eval)
    }
}
